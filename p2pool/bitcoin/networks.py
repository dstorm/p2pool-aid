import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

@defer.inlineCallbacks
def get_subsidy(bitcoind, target):
    res = yield bitcoind.rpc_getblock(target)

    defer.returnValue(res)

nets = dict(
    aidbit=math.Object(
        P2P_PREFIX='cab5edda'.decode('hex'),
        P2P_PORT=18881,
        ADDRESS_VERSION=23,
        RPC_PORT=18882,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'aidbitaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda bitcoind, target: get_subsidy(bitcoind, target),
        BLOCK_PERIOD=60,
        SYMBOL='AID',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'AidBit') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/AidBit/') if platform.system() == 'Darwin' else os.path.expanduser('~/.aidbit'), 'aidbit.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://columbus.aidbit.net/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://columbus.aidbit.net/address/',
        TX_EXPLORER_URL_PREFIX='http://columbus.aidbit.net/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=256,
        DUST_THRESHOLD=0.001e8,
        CHARITY1_ADDRESS='4c023fc67aedb2a8a2183f69a4239eb5a7b06ca6'.decode('hex'),
        CHARITY2_ADDRESS='9309693375dceb22952b86e86cea6b54afeff647'.decode('hex'),
    ),
    aidbit_testnet=math.Object(
        P2P_PREFIX='d9a4bea8'.decode('hex'),
        P2P_PORT=28881,
        ADDRESS_VERSION=111,
        RPC_PORT=28882,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'aidbitaddress' in (yield bitcoind.rpc_help()) and
            (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda bitcoind, target: get_subsidy(bitcoind, target),
        BLOCK_PERIOD=60, # s
        SYMBOL='AID',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'AidBit') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/AidBit/') if platform.system() == 'Darwin' else os.path.expanduser('~/.aidbit'), 'aidbit.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=256,
        DUST_THRESHOLD=0.001e8,
        CHARITY1_ADDRESS='8455ce43232f5ba7aacd88648e1ddad7fcd6aa10'.decode('hex'),
        CHARITY2_ADDRESS='689807e817b68819ce216c7d4edffd8bf82be81c'.decode('hex'),
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
